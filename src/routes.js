import Home from './components/Home.vue'
import DevelDoc from './components/DevelDoc.vue'
import UserDoc from './components/UserDoc.vue'

export default [
	{ path: '/', component: Home},	
	{ path: '/uzivatel', component: UserDoc},
	{ path: '/vyvojar', component: DevelDoc},
]