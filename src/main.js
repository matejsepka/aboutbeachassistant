import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueRouter from 'vue-router'
import Routes from './routes'

Vue.use(VueRouter)
Vue.use(BootstrapVue)

Vue.config.productionTip = false

export const bus = new Vue();
const router = new VueRouter({
	routes: Routes,
	/*mode: 'history'*/
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})